import { render, TemplateResult } from 'lit-html';
import { document, HTMLElement } from 'global';

export function withLitHtml(story) {
  return when(is(TemplateResult), renderAsSection, story());
}

function renderAsSection(template) {
	const  section = document.createElement('section');
	render(template, section);
	return section;
}

export function withClass(...classes) {
	return story => when(is(HTMLElement), ensureClasses(classes), story());
}

function ensureClasses(classes) {
	return element => {
		classes.forEach(c => element.classList.add(c));
		return element;
	}
}

/** Named as Ramda counterparts (https://ramdajs.com/docs/) */
function when(predicate, mapper, object) {
	return predicate(object) ? mapper(object) : object;
}

function is(Type) {
	return object => object instanceof Type;
}
