module.exports = async ({ config }) => {
	config.module.rules = [
		...config.module.rules,
		{ test: /\.(scss|sass)$/, use: ['raw-loader', 'sass-loader'] },
	];
	return config;
};
