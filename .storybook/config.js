import './style.css';
import { configure, addDecorator } from '@storybook/html';
import { withLitHtml, withClass } from './decorators';
import '../src/components';

addDecorator(withLitHtml);
addDecorator(withClass('story'));

configure(require.context('../src', true, /\.stories\.js$/), module);
