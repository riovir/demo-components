import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import sass from 'rollup-plugin-sass';
import visualizer from 'rollup-plugin-visualizer';
import { terser } from 'rollup-plugin-terser';
import { module as esModule, unpkg, library, dependencies } from '../package.json';
import { components } from './components.config';

const toInputName = without([/^dist\//, /\.js$/]);
const INDEX_FILE = 'src/components/index.js';

const plugins = [
	resolve(),
	commonjs(),
	babel(),
	sass(),
];

const esm = {
	input: { [toInputName(esModule)]: INDEX_FILE, ...components },
	output: { dir: 'dist', format: 'esm' },
	plugins,
	external
};

function external(dep) {
	return Object.keys(dependencies).includes(dep) || /lit-html/.test(dep);
}

const umd = {
	input: { [toInputName(unpkg)]: INDEX_FILE },
	output: { dir: 'dist', format: 'umd', name: library },
	plugins: [
		...plugins,
		terser(),
		visualizer({ open: !!process.env.ANALYZE }),
	],
	external: false
};
export default [esm, umd];

function without(parts) {
	return string => parts.reduce((result, part) => result.replace(part, ''), string);
}
