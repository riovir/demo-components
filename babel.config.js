module.exports = {
	presets: [
		['@babel/preset-env', {
			modules: false
		}]
	],
	plugins: [
		['ramda', { useES: true	}],
	]
};
