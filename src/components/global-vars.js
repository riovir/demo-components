import { cssVarOf } from './utils';

const globalVar = cssVarOf('demo');

export const transition = globalVar('transition', '0.4s ease-out');
