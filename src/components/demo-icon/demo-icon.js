import { LitElement, html, css } from 'lit-element';
import { IconRegistry } from './icon-registry';

const Private = {
	PATH: Symbol('path'),
	VIEW_BOX: Symbol('viewBox'),
};

export const Events = Object.freeze({
	ICONS_UPDATED: 'demo-icons-updated',
});

const registry = IconRegistry({ onChange: detail => {
	window.dispatchEvent(new CustomEvent(Events.ICONS_UPDATED, { detail }));
} });
export const addIcon = registry.addIcon;

export class DemoIcon extends LitElement {
	static get properties() {
		return {
			icon: { type: String },
			prefix: { type: String },
			def: { type: Object, reflect: false },
			[Private.VIEW_BOX]: { type: String, attribute: false },
			[Private.PATH]: { type: String, attribute: false },
		};
	}

	constructor() {
		super();
		Object.assign(this, {
			prefix: 'fas',
			icon: '',
			[Private.VIEW_BOX]: '0 0 2 2',
			[Private.PATH]: 'M1 1',
		});
		this.updateShape = this.updateShape.bind(this);
		this.updateShape();
	}

	connectedCallback() {
		super.connectedCallback();
		window.addEventListener(Events.ICONS_UPDATED, this.updateShape);
	}

	disconnectedCallback() {
		super.disconnectedCallback();
		window.removeEventListener(Events.ICONS_UPDATED, this.updateShape);
	}

	updated(props) {
		if (props.has('prefix') || props.has('icon')) {
			this.updateShape();
		}
	}

	updateShape() {
		const definition = this.def || registry.findIcon(iconMatching(this));
		const icon = parseFaIcon(definition);
		if (!icon) { return; }
		this[Private.VIEW_BOX] = icon.viewBox;
		this[Private.PATH] = icon.path;
	}

	render() {
		return html`
			<svg
				aria-hidden="true"
				role="img"
				focusable="false"
				xmlns="http://www.w3.org/2000/svg"
				viewBox=${this[Private.VIEW_BOX]}
			>
				<path fill="currentColor" d=${this[Private.PATH]} />
			</svg>
		`;
	}

	static get styles() {
		return css`
			:host {
				display: inline-block;
				width: 1.5rem;
				height: 1.5rem;
			}
			svg {
				display: block;
				width: 100%;
				height: 100%;
			}
		`;
	}
}

function iconMatching({ prefix, icon }) {
	return definition => definition.prefix === prefix && definition.iconName === icon;
}

function parseFaIcon({ icon = [] } = {}) {
	const [width, height, /* ligatures */, /* unicode */, path] = icon;
	if (!width || !height || !path) { return; }
	return { viewBox: `0 0 ${width} ${height}`, path };
}

customElements.define('demo-icon', DemoIcon);
