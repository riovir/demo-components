import { html } from 'lit-html';
import { faAnkh, faBrain, fas } from '@fortawesome/free-solid-svg-icons';
import { addIcon } from './demo-icon';

addIcon(faAnkh);

export default { title: 'demo-icon' };

const addIconSample =
`import { addIcon } from 'demo-components;
// Or...
import { addIcon } from 'demo-components/src/components/demo-icon;
// Import Font Awesome 5.1+ icon definitions one by one
import { faCar, faShip } from '@fortawesome/free-solid-svg-icons';
addIcon(faCar, faShip);
// Import Font Awesome 5.1+ icon definitions in bulk (warning: they are over 200KB!)
import { fas } from '@fortawesome/free-solid-svg-icons';
addIcon(fas);

// FYI the an icon definition looks something like this:
const anExample = {
	prefix: 'an',
	iconName: 'example',
	icon: [width, height, _ligatures, _unicode, svgPathData]
};`;

export const addIconApi = () => html`
	<demo-code .code=${addIconSample}></demo-code>
`;

export const iconProperty = () => html`
	<demo-icon icon="ankh"></demo-icon>
`;

export const addIconLater = () => html`
	<div>
		<button @click=${() => addIcon(faBrain)}>Call addIcon(faBrain);</button>
	</div>
	<demo-icon icon="brain"></demo-icon>
`;

export const addIconsInBulk = () => html`
	<div>
		<button @click=${() => addIcon(fas)}>Call addIcon(fas);</button>
	</div>
	<demo-icon id="the-icon" icon="ambulance"></demo-icon>
	<p>Type the name of any free, solid Font Awesome icon</p>
	<demo-string-knob selector="#the-icon" prop="icon" value="car" @keydown=${mute}></demo-string-knob>
`;

function mute(event) {
	event.stopPropagation();
}
