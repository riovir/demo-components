export function IconRegistry({ onChange }) {
	const library = { icons: [] };
	return {
		addIcon,
		findIcon,
		get icons() { return library.icons; }
	};

	function addIcon(...icons) {
		const addedIcons = unpackIconCollections(flattenDeep(icons));
		const areSameIcons = matchingProps(['prefix', 'iconName']);
		library.icons = [
			...addedIcons,
			...subtractBy(areSameIcons, library.icons, addedIcons)
		];
		onChange(library.icons);
	}

	function findIcon(...args) {
		return library.icons.find(...args);
	}
}

function unpackIconCollections(icons) {
	const asIcons = thing => isFaIcon(thing) ?
		[thing] :
		Object.values(thing).filter(isFaIcon);
	return icons.reduce((icons, iconOrCollection) => icons.concat(asIcons(iconOrCollection)), []);
}

function isFaIcon(object) {
	return typeof object.prefix === 'string' &&
		typeof object.iconName === 'string' &&
		object.icon && object.icon.length;
}

function flattenDeep(array = []) {
	return array.reduce((acc, val) => Array.isArray(val) ?
		acc.concat(flattenDeep(val)) :
		acc.concat(val), []);
}

function subtractBy(areSame, fromIcons, icons) {
	const notIn = array => a => !array.find(b => areSame(a, b));
	return fromIcons.filter(notIn(icons));
}

function matchingProps(props) {
	return (a, b) => !props.find(prop => a[prop] !== b[prop]);
}
