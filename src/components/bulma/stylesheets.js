import { maybeStylesheet } from './maybe-stylesheet';

/* BASE */

import { default as resetCss } from './styles/reset.scss';
export const reset = maybeStylesheet(resetCss);

/* ELEMENTS */

import { default as buttonCss } from './styles/button.scss';
export const button = maybeStylesheet(buttonCss);

/* COMPONENTS */

import { default as modalCss } from './styles/modal.scss';
export const modal = maybeStylesheet(modalCss);

import { default as navbarCss } from './styles/navbar.scss';
export const navbar = maybeStylesheet(navbarCss);
