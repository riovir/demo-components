import { supportsAdoptingStyleSheets } from './maybe-stylesheet';
import { reset } from './stylesheets';

export function withBulma(...widgetStyles) {
	const styles = [reset, ...widgetStyles];
	return ({ render, ...props }) => ({
		...props,
		_theme: supportsAdoptingStyleSheets ? adoptOnConnect(styles) : undefined,
		render: supportsAdoptingStyleSheets ? render : chainRenderWith(render, styles)
	});
}

function chainRenderWith(render, stringStyles) {
	return (...args) => render(...args).style(stringStyles);
}

function adoptOnConnect(constructedStyles) {
	return {
		connect({ render }) {
			render().adoptedStyleSheets = constructedStyles;
		}
	};
}
