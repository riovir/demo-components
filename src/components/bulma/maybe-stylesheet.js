export const supportsAdoptingStyleSheets =
    ('adoptedStyleSheets' in Document.prototype) &&
    ('replace' in CSSStyleSheet.prototype);

export function maybeStylesheet(css) {
	if (!supportsAdoptingStyleSheets) { return css; }
	const sheet = new CSSStyleSheet();
	sheet.replaceSync(css);
	return sheet;
}
