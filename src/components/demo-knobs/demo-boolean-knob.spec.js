import { expect, fixture, nextFrame, oneEvent } from '@open-wc/testing';
import './demo-boolean-knob';

describe('demo-boolean-knob', () => {
	it('has switch label with the checkbox role', async () => {
		const { knob } = await setup(`<demo-boolean-knob></demo-boolean-knob>`);
		const role = knob.shadowRoot
				.querySelector('.switch')
				.getAttribute('role');
		expect(role).to.equal('checkbox');
	});

	it('emits knob-change event on input', async () => {
		const { knob, input } = await setup(`<demo-boolean-knob></demo-boolean-knob>`);
		const changeEvent = oneEvent(knob, 'knob-change');
		input.click();
		expect(await changeEvent).to.exist;
	});

	it('controls both the prop and attribute of another element', async () => {
		const { subject } = await setup(`
			<demo-boolean-knob selector="#the-subject" prop="the-prop" value="true"></demo-boolean-knob>
			<div id="the-subject"></div>
		`);

		expect(subject['the-prop']).to.equal(true);
		expect(subject.getAttribute('the-prop')).to.equal('true');
	});

	it('controls a prop of another element', async () => {
		const { input, subject } = await setup(`
			<demo-boolean-knob selector="#the-subject" prop="the-prop"></demo-boolean-knob>
			<div id="the-subject"></div>
		`);

		input.click();
		await nextFrame();

		expect(subject['the-prop']).to.equal(true);
	});

	it('controls the content of another element if set', async () => {
		const { input, subject } = await setup(`
			<demo-boolean-knob selector="#the-subject" content></demo-boolean-knob>
			<div id="the-subject"></div>
		`);

		input.click();
		await nextFrame();

		expect(subject.innerHTML).to.equal('true');
	});

	it('element prop sets subject directly', async () => {
		const { knob, input } = await setup(`<demo-boolean-knob content></demo-boolean-knob>`);

		const subject = document.createElement('div');
		knob.element = subject;

		input.click();
		await nextFrame();

		expect(subject.innerHTML).to.equal('true');
	});
});

async function setup(template) {
	const el = await fixture(`<div>${template}</div>`);
	const knob = el.querySelector('demo-boolean-knob');
	const input = knob.shadowRoot.querySelector('input');
	const subject = el.querySelector('#the-subject');
	return { el, knob, input, subject };
}
