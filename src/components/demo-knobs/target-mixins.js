import { hasAny } from '../utils';

export const properties = Object.freeze({
	element: Object.freeze({ attribute: false }),
	selector: Object.freeze({ type: String, attribute: true }),
	prop: Object.freeze({ type: String, attribute: true }),
	content: Object.freeze({ type: Boolean, attribute: true }),
	value: Object.freeze({ type: String, attribute: true }),
});

const hasPropChange = hasAny(Object.keys(properties));

export function setup(component, { eventToValue = event => event.target.value } = {}) {
	return { updateTarget, onValueInput };

	function updateTarget(changedProperties) {
		if (!shouldUpdateTarget({ changedProperties })) { return; }
		const target = resolveTarget(component);
		if (!target) { return; }
		const { prop, content, value } = component;
		if (content) {
			target.innerHTML = String(value);
			return;
		}
		target.setAttribute(prop, value);
		target[prop] = value;
	}

	function onValueInput(event) {
		const value = eventToValue(event);
		component.value = value;
		component.dispatchEvent(new CustomEvent('knob-change', { value }));
		event.stopPropagation();
	}
}

function shouldUpdateTarget({ changedProperties }) {
	if (changedProperties === undefined) { return true; }
	return hasPropChange(changedProperties);
}

function resolveTarget({ element, selector }) {
	return element || document.querySelector(selector);
}
