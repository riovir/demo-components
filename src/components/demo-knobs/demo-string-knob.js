import { LitElement, html, css } from 'lit-element';
import { properties, setup } from './target-mixins';

export class DemoStringKnob extends LitElement {
	static get properties() {
		return properties;
	}

	constructor() {
		super();
		const { updateTarget, onValueInput } = setup(this);
		Object.assign(this, { value: '' });
		Object.assign(this, { updateTarget, onValueInput });
	}

	updated(changedProperties) {
		this.updateTarget(changedProperties);
	}

	render() {
		return html`
  	<input type="text" @input=${this.onValueInput} value=${this.value}>
		`;
	}

	static get styles() {
		return css`
			:host {
				display: block;
			}
		`;
	}
}
customElements.define('demo-string-knob', DemoStringKnob);
