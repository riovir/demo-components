import { html } from 'lit-html';

export default { title: 'demo-knobs' };

export const booleanProp = () => html`
	<demo-boolean-knob selector="#subject" prop="checked"></demo-boolean-knob>
	<h3>Target element</h3>
	<input id="subject" type="checkbox" value="true" />
`;

export const stringProp = () => html`
	<demo-string-knob selector="#subject" prop="value"  @keydown=${mute}></demo-string-knob>
	<h3>Target element</h3>
	<input id="subject" type="text" />
`;

export const targetingInnerHTML = () => html`
	<demo-string-knob selector="#subject" content @keydown=${mute}></demo-string-knob>
	<demo-boolean-knob selector="#subject" .value=${false} content></demo-boolean-knob>
	<h3>Target element</h3>
	<div id="subject"></div>
`;

function mute(event) {
	event.stopPropagation();
}
