import { LitElement, html, css } from 'lit-element';
import { transition } from '../global-vars';
import { cssVarOf } from '../utils';
import { properties, setup } from './target-mixins';

const cssVar = cssVarOf('demo-boolean-knob');

export class DemoBooleanKnob extends LitElement {
	static get properties() {
		return {
			...properties,
			value: { ...properties.value, type: Boolean }
		};
	}

	constructor() {
		super();
		const { updateTarget, onValueInput } = setup(this, { eventToValue });
		Object.assign(this, { updateTarget, onValueInput });
	}

	updated(changedProperties) {
		this.updateTarget(changedProperties);
	}

	render() {
		return html`
		<label class="switch" role="checkbox">
  		<input type="checkbox" @input=${this.onValueInput} ?checked=${this.value}>
			<div class="slider"></div>
			<span><slot></slot></span>
		</label>
		`;
	}

	static get styles() {
		const sliderHeight = cssVar('slider-height', '2rem');
		const dotSize = cssVar('dot-size', '1.5rem');

		return css`
			:host {
				display: block;
			}

			.switch {
				display: block;
				width: ${cssVar('slider-width', '4rem')};
				height: ${sliderHeight};
			}

			.switch input {
				display: block;
				opacity: 0;
				width: 0;
				height: 0;
			}

			.slider {
				display: inline-flex;
				align-items: center;
				cursor: pointer;
				width: 100%;
				height: 100%;
				border-radius: ${sliderHeight};
				background-color: ${cssVar('slider-color', '#ccc')};
				transition: ${transition};
			}

			.slider:before {
				display: block;
				content: "";
				flex-grow: 0;
				transition: ${transition};
			}

			.slider:after {
				display: block;
				content: "";
				height: ${dotSize};
				width: ${dotSize};
				margin: 0 calc((${sliderHeight} - ${dotSize}) / 2);
				border-radius: 50%;
				background-color: ${cssVar('slider-dot-color', 'white')};
			}

			input:checked + .slider {
				background-color: ${cssVar('slider-checked-color', '#2196F3')};;
			}

			input:focus + .slider {
				box-shadow: ${cssVar('slider-focus-shadow', ' 0 0 0.25rem #2196F3')};
			}

			input:checked + .slider:before {
				flex-grow: 1;
			}
		`;
	}
}

customElements.define('demo-boolean-knob', DemoBooleanKnob);

function eventToValue(event) {
	return event.target.checked;
}
