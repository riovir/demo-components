import { expect, fixture, oneEvent } from '@open-wc/testing';
import './demo-string-knob';

describe('demo-string-knob', () => {
	it('emits knob-change event on input', async () => {
		const { knob, input } = await setup(`<demo-string-knob></demo-string-knob>`);
		const changeEvent = oneEvent(knob, 'knob-change');
		type('hello', input);
		expect(await changeEvent).to.exist;
	});
});

async function setup(template) {
	const el = await fixture(`<div>${template}</div>`);
	const knob = el.querySelector('demo-string-knob');
	const input = knob.shadowRoot.querySelector('input');
	const subject = el.querySelector('#the-subject');
	return { el, knob, input, subject };
}

function type(text, el) {
	el.value = text;
	el.dispatchEvent(new Event('input', {
		bubbles: true,
		cancelable: true,
	}));
}
