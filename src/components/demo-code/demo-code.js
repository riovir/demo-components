import { LitElement, html, css} from 'lit-element';
import { complement, drop, dropWhile, join, pipe, split, splitWhen, take, test } from 'ramda';
import { hasAny } from '../utils';
import { okaidia } from './internal-themes';
import prism from 'prismjs';

export { prism };
const { highlightElement } = prism;

const hasCodeRefChange = hasAny(['codeHref']);
const hasCodeChange = hasAny([
	'lang', 'code',
	'fromLine', 'toLine',
	'fromRegex', 'toRegex',
	'lines'
]);

export class DemoCode extends LitElement {
	static get properties() {
		return {
			lang: { type: String, attribute: true },
			code: { type: String, attribute: true },

			codeHref: { type: String, attribute: 'code-href' },
			fromLine: { type: Number, attribute: 'from-line' },
			fromRegex: { type: String, attribute: 'from-regex' },
			toLine: { type: Number, attribute: 'to-line' },
			toRegex: { type: String, attribute: 'to-regex' },
			lines: { type: Number, attribute: true },

			themeHref: { type: String, attribute: 'theme-href' },
		};
	}

	constructor() {
		super();
		Object.assign(this, {
			code: '"..."',
			lang: 'js',
		});
	}

	async updated(changedProperties) {
		if (hasCodeChange(changedProperties)) { this.renderSyntax(); }
		if (hasCodeRefChange(changedProperties)) {
			this.code = await fetchCode(this);
		}
	}

	renderSyntax() {
		const preElement = this.shadowRoot.querySelector('pre');
		if (!preElement || !this.code) { return; }
		const visibleCode = trimCode(this);
		preElement.innerHTML = `<code>${visibleCode}</code>`;
		highlightElement(preElement);
	}

	render() {
		return html`
			${this.themeHref ? html`<link rel="stylesheet" href="${this.themeHref}">` : null}
			<div class="syntax ${this.themeHref ? '' : 'has-internal-theme'}">
				<pre class="${`language-${this.lang}`}">
						<!-- Content injected by renderSyntax -->
					</pre>
			</div>
		`;
	}

	static get styles() {
		return css`
			:host {
				display: block;
				width: max-content;
				height: max-content;
				max-width: 100%;
				max-height: 32rem;
			}
			.syntax {
				max-width: inherit;
				max-height: inherit;
				margin: 0;
				padding: 0;
			}
			.syntax pre {
				max-width: inherit;
				max-height: inherit;
				box-sizing: border-box;
				margin: 0 !important;
				overflow: auto;
			}
			${okaidia}
		`;
	}
}

customElements.define('demo-code', DemoCode);

async function fetchCode({ code, codeHref }) {
	if (!codeHref) { return code; }
	try {
		const response = await fetch(codeHref);
		return await response.text();
	}
	catch (err) {
		return '"(X_X)"';
	}
}

/** Man, I should really unit test this part... as well... */
function trimCode({ code, fromLine, fromRegex, toLine, toRegex, lines }) {
	if (!code || !code.split) { return code; }
	return pipe(
			split('\n'),
			dropFirst({ fromLine, fromRegex }),
			keepLast({ fromLine, toLine, toRegex }),
			keepLast({ lines }),
			join('\n')
	)(code);
}

function dropFirst({ fromLine, fromRegex }) {
	if (0 <= fromLine) { return drop(fromLine - 1); }
	if (fromRegex) { return dropWhile(notMatching(fromRegex)); }
	return rows => rows;
}

function keepLast({ fromLine, toLine, toRegex, lines }) {
	const amount = 0 <= lines ? lines : toLine - fromLine;
	if (0 <= amount) { return take(amount); }
	if (toRegex) {
		return pipe(
				splitWhen(isMatching(toRegex)), // Split array at match
				concatFirst // But also include the match
		);
	}
	return rows => rows;
}

function concatFirst([leftArray, rightArray]) {
	return rightArray.length ? leftArray.concat(rightArray[0]) : leftArray;
}

function isMatching(pattern) {
	const regexp = pattern.test ? pattern : new RegExp(pattern);
	return test(regexp);
}

function notMatching(pattern) {
	return complement(isMatching(pattern));
}
