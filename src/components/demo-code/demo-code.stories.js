import { html } from 'lit-html';

export default { title: 'demo-code' };

export const blank = () => html`
	<demo-code></demo-code>
`;

export const fetchingCode = () => html`
	<demo-code
		code-href="https://raw.githubusercontent.com/jgthms/bulma/master/sass/utilities/mixins.sass"
		from-regex="hamburger"
		to-regex="^$"
		lang="css"
	></demo-code>
`;

export const fetchingError = () => html`
	<demo-code code-href="https://42"></demo-code>
`;

export const prismThemeSupport = () => html`
	<demo-code
		code=".is-danger { color: red; }"
		lang="css"
		theme-href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.17.1/themes/prism.min.css"
	></demo-code>
`;


