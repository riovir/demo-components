import { css } from 'lit-element';

/**!
 * @preserve okaidia theme for JavaScript, CSS and HTML
 * Loosely based on Monokai textmate theme by http://www.monokai.nl/
 * @author ocodia
 * Changes: added .has-internal-theme and removed unused rules.
 */
export const okaidia = css`
	.has-internal-theme code[class*="language-"],
	.has-internal-theme pre[class*="language-"] {
		color: #f8f8f2;
		background: none;
		text-shadow: 0 1px rgba(0, 0, 0, 0.3);
		font-family: Consolas, Monaco, 'Andale Mono', 'Ubuntu Mono', monospace;
		font-size: 1em;
		text-align: left;
		white-space: pre;
		word-spacing: normal;
		word-break: normal;
		word-wrap: normal;
		line-height: 1.5;

		-moz-tab-size: 4;
		-o-tab-size: 4;
		tab-size: 4;

		-webkit-hyphens: none;
		-moz-hyphens: none;
		-ms-hyphens: none;
		hyphens: none;
	}

	/* Code blocks */
	.has-internal-theme pre[class*="language-"] {
		padding: 1em;
		margin: .5em 0;
		overflow: auto;
		border-radius: 0.3em;
	}

	.has-internal-theme :not(pre) > code[class*="language-"],
	.has-internal-theme pre[class*="language-"] {
		background: #272822;
	}

	/* Inline code */
	.has-internal-theme :not(pre) > code[class*="language-"] {
		padding: .1em;
		border-radius: .3em;
		white-space: normal;
	}

	.has-internal-theme .token.cdata,
	.has-internal-theme .token.comment,
	.has-internal-theme .token.doctype,
	.has-internal-theme .token.prolog {
		color: #708090
	}

	.has-internal-theme .token.punctuation {
		color: #f8f8f2
	}

	.has-internal-theme .namespace {
		opacity: .7
	}

	.has-internal-theme .token.constant,
	.has-internal-theme .token.deleted,
	.has-internal-theme .token.property,
	.has-internal-theme .token.symbol,
	.has-internal-theme .token.tag {
		color: #f92672
	}

	.has-internal-theme .token.boolean,
	.has-internal-theme .token.number {
		color: #ae81ff
	}

	.has-internal-theme .token.attr-name,
	.has-internal-theme .token.builtin,
	.has-internal-theme .token.char,
	.has-internal-theme .token.inserted,
	.has-internal-theme .token.selector,
	.has-internal-theme .token.string {
		color: #a6e22e
	}

	.has-internal-theme .language-css .token.string,
	.has-internal-theme .style .token.string,
	.has-internal-theme .token.entity,
	.has-internal-theme .token.operator,
	.has-internal-theme .token.url,
	.has-internal-theme .token.variable {
		color: #f8f8f2
	}

	.has-internal-theme .token.atrule,
	.has-internal-theme .token.attr-value,
	.has-internal-theme .token.class-name,
	.has-internal-theme .token.function {
		color: #e6db74
	}

	.has-internal-theme .token.keyword {
		color: #66d9ef
	}

	.has-internal-theme .token.important,
	.has-internal-theme .token.regex {
		color: #fd971f
	}

	.has-internal-theme .token.bold,
	.has-internal-theme .token.important {
		font-weight: 700
	}

	.has-internal-theme .token.italic {
		font-style: italic
	}

	.has-internal-theme .token.entity {
		cursor: help
	}
`;
