import { html as h, define } from 'hybrids';
import { withBulma, navbar } from '../bulma';

const theme = withBulma(navbar);

export const BNavbarItem = theme({
	href: '',
	target: '',
	render: ({ href, target }) => h`
		${href ? Anchor({ href, target }) : Div()}
  `,
});

function Anchor({ href, target }) {
	return h`
		<a class="navbar-item" href="${href}" target="${target}">
			<slot></slot>
		</a>`;
}

function Div() {
	return h`
		<div class="navbar-item">
			<slot></slot>
		</div>`;
}

define({ BNavbarItem });
