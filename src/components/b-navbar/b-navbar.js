import { html as h, define } from 'hybrids';
import { withBulma, navbar } from '../bulma';

const theme = withBulma(navbar);

export function toggleMenu(host) {
	host.menuActive = !host.menuActive;
}

export const BNavbar = theme({
	ariaLabel: 'main navigation',
	burgerAriaLabel: 'menu',
	menuActive: false,
	menuId: `the-menu`,
	render: ({ ariaLabel, burgerAriaLabel, menuActive, menuId }) => h`
		<nav class="navbar" role="navigation" aria-label="${ariaLabel}">
			<div class="navbar-brand">
				<slot name="brand"></slot>

				<button
					class="navbar-burger burger ${menuActive ? 'is-active' : ''}"
					aria-label="${burgerAriaLabel}"
					aria-expanded="${menuActive ? 'true' : 'false'}"
					data-target="${menuId}"
					onclick="${toggleMenu}"
				>
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
					<span aria-hidden="true"></span>
				</button>
			</div>

			<div id="${menuId}" class="navbar-menu ${menuActive ? 'is-active' : ''}">
				<div class="navbar-start">
					<slot></slot>
				</div>

				<div class="navbar-end">
					<slot name="end"></slot>
				</div>
			</div>
		</nav>
  `,
});

define({ BNavbar });
