import { html } from 'lit-html';

export default { title: 'b-navbar' };

export const example = () => html`
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">

	<b-navbar>
		<b-navbar-item slot="brand" href="#">
			<img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
		</b-navbar-item>

		<b-navbar-item href="#">Home</b-navbar-item>
		<b-navbar-item href="#">Documentation</b-navbar-item>

		<div slot="end" class="navbar-item">
			<div class="buttons">
				<button class="button is-primary">
					<strong>Sign up</strong>
				</button>
				<button class="button is-light">
					Log in
				</button>
			</div>
		</div>
	</b-navbar>
`;

export const colorsAreAProblem = () => html`
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<b-navbar class="is-dark">
		<b-navbar-item slot="brand" href="https://github.com/jgthms/bulma/blob/master/sass/components/navbar.sass#L60-L67" target="_blank">
			Whoops
		</b-navbar-item>
	</b-navbar>

	<div class="notification is-warning">
		The Shadow DOM cuts the ".navbar.is-dark .navbar-item" selectors in half. Twice...
	</div>
`;

const items = range(1000);

export const manyNavbarItemDivs = () => html`
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	${items.map(i => html`<div class="navbar-item">Item ${i}</div>`)}
`;

export const manyNavbarItemComponents = () => html`
	${items.map(i => html`<b-navbar-item href="#">Item ${i}</b-navbar-item>`)}
`;

function range(size, startAt = 0) {
	return [...Array(size).keys()].map(i => i + startAt);
}
