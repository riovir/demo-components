import { expect } from '@open-wc/testing';
import { hasAny } from './has-any';

describe('has-any', () => {
	it('supports auto-currying', () => {
		const container = [1, 2, 3];
		const numbers = [2, 42];
		expect(hasAny(numbers, container)).to.equal(true);
		expect(hasAny(numbers)(container)).to.equal(true);
	});
});
