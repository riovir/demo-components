import { unsafeCSS } from 'lit-element';

export function cssVarOf(component) {
	return (name, fallback) => unsafeCSS(`var(--${component}-${name}, ${fallback})`);
}
