export function hasAny(items, container) {
	if (container === undefined) {
		return (container = []) => hasAny(items, container);
	}
	if (!items || !items.some) { return false; }

	const isFound = container.has ?
		item => container.has(item) :
		item => container.includes(item);
	return items.some(isFound);
}
