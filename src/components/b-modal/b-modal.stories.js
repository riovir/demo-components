import { html } from 'lit-html';

export default { title: 'b-modal' };

export const example = () => html`
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
	<b-modal active>
		<div class="box">Hello</div>
	</b-modal>
`;

export const withKeepOpen = () => html`
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">

	<b-modal active keep-open @close=${log}>
		<div class="box">
			<h1 class="title">Hello</h1>
			<div class="content" data-logs></div>
			<button class="button" @click=${deactivate}>Just close it</button>
		</div>
	</b-modal>
`;

export const focusTrapping = () => html`
	<style>
		button:focus {
			border-width: 4px;
		}
	</style>
	<button>Duck</button>
	<button>Duck</button>
	<button @click=${activate}>Show modal</button>
	<button>Goose</button>

	<b-modal>
		<div style="padding: 2rem 1rem; background: white;">
			<button>Left</button>
			<button>Right</button>
		</div>
	</b-modal>
`;

function activate() {
	document.querySelector('b-modal').active = true;
}

function deactivate() {
	document.querySelector('b-modal').active = false;
}

function log({ type }) {
	const eventLog = document.createElement('div');
	eventLog.innerHTML = `<em>Got "${type}" event!</em>`;
	document.querySelector('[data-logs]').appendChild(eventLog);
}
