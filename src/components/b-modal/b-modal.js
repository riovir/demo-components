import { html as h, define, dispatch, property } from 'hybrids';
import moveFocusInside, { focusInside } from 'focus-lock';
import { withBulma, modal } from '../bulma';

const theme = withBulma(modal);
export const BModal = theme({
	keepOpen: false,
	closeButton: ref('.modal-close'),
	active: {
		...property(false),
		observe: (host, value) => value ?
			saveFocus(host) :
			returnFocus(host),
	},
	lastFocusedElement: {
		set: whenActive,
	},
	lifecycle: {
		connect: initFocusTrap
	},
	render: ({ active }) => h`
		<div
			class="modal ${active ? 'is-active' : ''}"
			onkeydown="${onKey(isEsc, attemptClosing)}"
		>
			<div class="modal-background"></div>
			<button
				class="modal-close is-large"
				aria-label="close"
				onclick="${attemptClosing}"
			></button>
			<div class="modal-content">
				<slot></slot>
			</div>
		</div>
  `,
});

define({ BModal });

function initFocusTrap(host) {
	const ensureValidFocus = trapFocusOnActive(host);
	document.addEventListener('focusin', ensureValidFocus);
	return () => document.removeEventListener('focusin', ensureValidFocus);
}

function trapFocusOnActive(host) {
	return () => {
		if (!host.active || focusInside(host)) { return; }
		moveFocusInside(host);
	};
}

function attemptClosing(host) {
	if (!host.active) { return; }
	dispatch(host, 'close');
	if (host.keepOpen) { return; }
	host.active = false;
}

function whenActive({ active }, value) {
	return active ? value : undefined;
}

function returnFocus(host) {
	if (!host.lastFocusedElement) { return; }
	host.lastFocusedElement.focus();
}

function saveFocus(host) {
	host.lastFocusedElement = document.activeElement;
	host.closeButton.focus();
}

/** GENERAL UTILS to be extracted */

function ref(selector) {
	return ({ render }) => render().querySelector(selector);
}

function onKey(predicate, handler) {
	return (host, event) => predicate(event) ? handler(host, event) : undefined;
}

function isEsc({ key, keyCode }) {
	const KEYCODE_ESC = 27;
	return key === 'Esc' || keyCode === KEYCODE_ESC;
}
