const { createDefaultConfig } = require('@open-wc/testing-karma');
const puppeteer = require('puppeteer');
const merge = require('deepmerge');

process.env.CHROME_BIN = puppeteer.executablePath();

module.exports = config => {
	config.set(
			merge(createDefaultConfig(config), {
				files: [
					{ pattern: config.grep ? config.grep : 'src/**/*.spec.js', type: 'module' },
				],

				esm: { nodeResolve: true },
			}),
	);
	return config;
};
