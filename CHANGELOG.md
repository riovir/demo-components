# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

Before `v1.0` breaking changes are reflected in the `minor` version.


## [0.2.3] - 2019-09-12
### Fixed
- **demo-code** fix `max-width` and scrolling.

## [0.2.2] - 2019-09-12
### Fixed
- **demo-code** type of `from-regex` and `to-regex` should have been a string.

## [0.2.1] - 2019-09-12
### Added
- **demo-code** support for matching `from-regex` and `to-regex`.

## [0.2.0] - 2019-09-11
### Added
- **demo-code** support for trimming code down to lines. Not an API breaking change, but hoped to bust the cache of the Pika CDN.

## [0.1.3] - 2019-09-11
### Fixed
- **demo-code** missing multiline code support.

## [0.1.2] - 2019-09-11
### Fixed
- **demo-code**'s buggy reliance on `unsafeHTML` directive from `lit-html`.

## [0.1.1] - 2019-09-11
### Another experimental release
- There is not even a proper entry for it.

## [0.1.0] - 2019-09-10
### Initial release (experimental)

This release is exclusively targeting a particular workshop.
